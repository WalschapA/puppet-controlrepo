class website {
  $website_path = '/var/www/html/index.html'

  package { 'apache2':
    ensure => present,
    before => File[$website_path],
  }

  $website_hash = {
    'nodeidentifyer' => $facts['fqdn'],
  }

  file { $website_path:
    content => epp('website/index.html.epp', $website_hash),
    notify  => Service['apache2'],
  }

  service { 'apache2':
    ensure => running,
  }
}
